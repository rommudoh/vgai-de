#!/usr/bin/env bash
git pull 2>&1 >> ~/git_pull.log \
&& git add mentions/mentions.csv 2>&1 >> ~/git_add.log \
&& git commit -m "update mentions csv" --no-gpg-sign 2>&1 >> ~/git_commit.log \
&& git push 2>&1 >> ~/git_push.log
